import { Tabs , PullToRefresh } from 'antd-mobile';
import axios from 'axios'
import React, { Component } from 'react';
import ReactDOM from 'react-dom'
import perR from '../images/perR.png'

export default class myTabs extends Component {
    constructor(props){
        super(props)
        this.state={
            userId:window.localStorage.userId,
            organizationId:window.localStorage.organizationId,
            items:[],
            pageNum:1,
            refreshing: false,  //是否显示刷新状态
            down: false,
            height: document.documentElement.clientHeight,  //浏览器高度
        };
    }
    componentDidMount(){
        const hei = this.state.height - ReactDOM.findDOMNode(this.ptr).offsetTop;
        setTimeout(() => this.setState({
            height: hei,
        }), 0);
        this.getZXmessage()
    }
    getZXmessage=()=>{
        var that=this;
        axios({     //装修知识数据的请求
            method:"POST",
            headers:{'Content-type':'application/json'},
            url:'http://app.zhuangneizhu.com/user/gainOfficialAndArticleMessage.do',
            params:{
                version:"3.0",
                organizationId:this.state.organizationId,
                userId:this.state.userId,
                type:2,
                pageNum:this.state.pageNum,
                pageSize:5,
                messageId:'',
            }
        })
        .then(function (res) {
            that.setState({
                items:res.data.data.items
            })
        })
        .catch(function (error) {
            console.log(error);
        });
    }
    handleClick(){
        console.log(1)
        // window.location.href=`http://admin.zhuangneizhu.com/admin/showArticle.do?aid=${this.state.messageId}`
    }
    render(){
        const tabs = [
            { title:'官方消息' },
            { title:'装修知识' },
        ];
        return(
            <Tabs tabs={tabs}
                initialPage={1}
                onChange={(tab, index) => { console.log('onChange', index, tab); }}
                onTabClick={(tab, index) => { console.log('onTabClick', index, tab); }}
                >
                {/* 官方知识列表 */}
                <div style={{ display: 'flex', height: '150px', backgroundColor: '#f5f8ff' }}>
                    Content of first tab
                </div>
                {/* 装修知识列表 */}
                <div style={{ display: 'flex',flexDirection:'column', backgroundColor: '#f5f8ff',paddingBottom:'50px' }}> 
                {/* 列表下拉刷新 */}
                <PullToRefresh
                    damping={60}  //拉动距离限制, 建议小于 200
                    ref={el => this.ptr = el}
                    style={{height: this.state.height,overflow: 'auto',paddingBottom:50}}
                    indicator={this.state.down ? {} : { deactivate: '上拉可以刷新' }}
                    direction={this.state.down ? 'down' : 'up'}
                    refreshing={this.state.refreshing}
                    onRefresh={() => {
                        this.setState({ refreshing: true ,pageNum:++this.state.pageNum},()=>{
                            this.getZXmessage();
                        });
                        setTimeout(() => {
                            this.setState({ refreshing: false });
                        }, 1000);
                        console.log(this.state.pageNum)
                    }}
                >
                    {this.state.items.map ((item, index) =>
                        <div key={index} style={{padding:'0.22rem 0.3rem 0 0.3rem',marginBottom:'0.2rem',backgroundColor:'#fff'}} onClick={this.handleClick}>
                            <h2 style={{fontSize:'0.34rem',lineHeight:'0.42rem',color:'#252525'}}>{item.content}</h2>
                            <div style={{lineHeight:'0.62rem',fontSize:'0.26rem',color:'#5c5c5c',paddingBottom:'0.16rem'}}>{"发布时间 "+item.createTime+' '+item.operName}</div>
                            <div><img src={item.photo} alt={item.messageId} style={{width:'6.86rem',height:'3.6rem',paddingBottom:'0.08rem',borderBottom:'1px solid #e6e6e6'}}/></div>
                            <div style={{position:'relative'}}>
                                <div style={{lineHeight:'0.86rem',color:'#363636'}}>查看详情</div>
                                <div><img src={perR} alt="右箭头" style={{width:'0.15rem',height:'0.23rem',position:'absolute',right:'0.05rem',top:'0.3rem'}}/></div>
                            </div>
                        </div>
                    )}
                    </PullToRefresh>
                </div>
                
            </Tabs>
        )
    }     
}