import {NavBar , Icon  }    from 'antd-mobile';
import React, { Component } from 'react';

export default class Navbar extends Component {
    constructor(props) {
      super(props);
      this.state = {
    
      };
    }
    render() {
      return (
        <div> 
          <NavBar
            mode="dark"
            leftContent="Back"
            rightContent={[
              <Icon key="0" type="search" style={{ marginRight: '16px' }} />,
              <Icon key="1" type="ellipsis" />,
            ]}
          >NavBar
          </NavBar>     
        </div>
      );
    }
  }