import { TabBar } from 'antd-mobile';
import { hashHistory } from 'react-router'
import React from 'react';
import fot1a from '../images/fot1-1.jpg'
import fot1b from '../images/fot1-2.jpg'
import fot2a from '../images/fot2-1.jpg'
import fot2b from '../images/fot2-2.jpg'
import fot3a from '../images/fot3-1.jpg'
import fot3b from '../images/fot3-2.jpg'
import fot4a from '../images/fot4-1.jpg'
import fot4b from '../images/fot4-2.jpg'
import fot5a from '../images/fot5-1.jpg'
import fot5b from '../images/fot5-2.jpg'
export default class Tabbar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedTab: 'projectTab',
      fullScreen: false,
    };
  }

  render() {
    const iconStyle={width:'24px',height:'24px'}
    return (
      <div style={{ position: 'fixed', width: '100%', bottom: 0 }}>
        <TabBar unselectedTintColor="#949494" tintColor="#33A3F4" barTintColor="#fff">
          <TabBar.Item
            title="项目"
            key="project"
            icon={<img src={fot1a} style={iconStyle} alt="项目"/>}
            selectedIcon={<img src={fot1b} style={iconStyle} alt="项目"/>}
            selected={this.state.selectedTab === 'projectTab'}
            onPress={() => {
              this.setState({
                selectedTab: 'projectTab',
              },()=>{
                hashHistory.push('/main')
              });
            }}
          >
          </TabBar.Item>
          <TabBar.Item 
            icon={<img src={fot2a} style={iconStyle} alt="消息"/>}
            selectedIcon={<img src={fot2b} style={iconStyle} alt="消息"/>}
            title="消息"
            key="message"
            selected={this.state.selectedTab === 'messageTab'}
            onPress={() => {
              hashHistory.push({pathname: '/message'})
              this.setState({
                selectedTab: 'messageTab',
              });
            }}
          >
          </TabBar.Item>
          <TabBar.Item
            icon={<img src={fot3a} style={iconStyle} alt="新建项目"/>}
            selectedIcon={<img src={fot3b} style={iconStyle} alt="新建项目"/>}
            title="新建项目"
            key="addProject"
            selected={this.state.selectedTab === 'greenTab'}
            onPress={() => {
              hashHistory.push('/addProject')
              this.setState({
                selectedTab: 'greenTab',
              });
            }}
          >
          </TabBar.Item>
          <TabBar.Item
            icon={<img src={fot4a} style={iconStyle} alt="成员"/>}
            selectedIcon={<img src={fot4b} style={iconStyle} alt="成员"/>}
            title="成员"
            key="member"
            selected={this.state.selectedTab === 'yellowTab'}
            onPress={() => {
              hashHistory.push('/member')
              this.setState({
                selectedTab: 'yellowTab',
              });
            }}
          >
          </TabBar.Item>
          <TabBar.Item
            icon={<img src={fot5a} style={iconStyle} alt="我的"/>}
            selectedIcon={<img src={fot5b} style={iconStyle} alt="我的"/>}
            title="我的"
            key="myself"
            selected={this.state.selectedTab === 'myselfTab'}
            onPress={() => {
              hashHistory.push('/myself')
              this.setState({
                selectedTab: 'myselfTab',
              });
            }}
          >
          </TabBar.Item>
        </TabBar>
      </div>
    );
  }
}