import './index.less';
import React, { Component } from 'react'
import ReactDOM from 'react-dom';
import registerServiceWorker from './registerServiceWorker';
import Tabbar from './components/TabBar.js'
import {Router, Route, hashHistory,IndexRoute} from 'react-router';

import Project          from './pages/project'
import Message          from './pages/message/message'
import AddProject       from './pages/addProject/addProject'
import Member           from './pages/member'
import Myself           from './pages/myself/myself'
import Login            from './pages/login/login'
import SetMeterial      from './pages/setMeterial/setMeterial'

class App extends Component {
  render() {
    return (
      <div className="container">
            {this.props.children}
        <Tabbar/>
      </div>   
    );
  }
}

ReactDOM.render(
    <Router history={ hashHistory }>
        <Route path="/"        component={Login} />
        <Route path="/main"    component={App}>
            <IndexRoute component={Project}/>
            <Route path="/project"       component={Project} />
            <Route path="/message"       component={Message} />
            <Route path="/addProject"    component={AddProject} />
            <Route path="/member"        component={Member} />
            <Route path="/myself"        component={Myself} /> 
        </Route> 
        <Route path="/setMeterial"        component={SetMeterial} /> 
    </Router>
    , document.getElementById('root'));
registerServiceWorker();
