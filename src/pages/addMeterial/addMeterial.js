import { Component } from 'react';
import './addMeterial.less';
const left = require("../../images/left.png")
import axios from 'axios'
export default class AddMeterial extends Component {
  constructor(props) {
    super(props);
  }
  handleReturn(){
    history.go(-1);
  }
  handleSure(){
    axios({     
      method:"GET",
      headers:{'Content-type':'application/json'},
      url:'http://app.zhuangneizhu.com/set/addMaterialsType.do',
      params:{
          organizationId:globel.organizationId,  //暂时这么写
          materialsName:globel.materialsName,
          userId:this.data.userId,
          version:globel.version,
      }
    })
    .then(function (res) {
        console.log(res);
    })
    .catch(function (error) {
        console.log(error);
    });
  }
  render() {
    return (
      <div className="a-container">
            <div className="a-top">
                <div className="img" onClick={this.handleReturn}><img src={left}/></div>
                <div className="title">添加建材名称</div>
                <div></div>
            </div>
            <input type="text" placeholder="输入建材名称" className="a-input"/>
            <button className="a-sure" onClick={this.handleSure}>确定</button>
      </div>
    );
  }
}