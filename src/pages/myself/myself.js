import React,{ Component } from 'react';
import { hashHistory } from 'react-router'
import { List } from 'antd-mobile';
import bottom from '../../images/bottom.png'
import './myself.less';
const perlogo =   require('../../images/perlogo.png') 
const perR    =   require('../../images/perR.png')
const per01   =   require('../../images/per01.png')
const per02   =   require('../../images/per02.png')
const per03   =   require('../../images/per03.png')
const per04   =   require('../../images/per04.png')
const per05   =   require('../../images/per05.png')
const per06   =   require('../../images/per06.png')
const per07   =   require('../../images/per07.png')
const per08   =   require('../../images/per08.png')
const per09   =   require('../../images/per09.png')
const per10   =   require('../../images/per10.png')
const per11   =   require('../../images/per11.png')
const per12   =   require('../../images/per12.png')
const per13   =   require('../../images/per13.png')
const per14   =   require('../../images/per14.png')
const per15   =   require('../../images/per14.png')

export default class Myself extends Component {
    constructor(props){
        super(props);
        this.state={
            display:'none',
        }
    }
    handleHidden=()=>{
       if(this.state.display==='none'){
           this.setState({
                display:'block', 
           })
       }else{
            this.setState({
                display:'none', 
            })
       }
    }
    setMeterials(){
        hashHistory.push('/setMeterial')
    }
    render() {
        return (
            <div className="page">
                <div className="top" onClick={this.handleHidden}>
                    <span>测试公司</span>
                    <img src={bottom} alt="下箭头"/>
                </div>
                <List className="my-list myself-list" style={{display:this.state.display}}>
                    <List.Item>content 1</List.Item>
                    <List.Item>content 2</List.Item>
                    <List.Item>content 3</List.Item>
                </List>
                <div className="header">
                    <div className="h-left">
                        <img alt='' className="logo" src={perlogo}/>
                        <div className="job">
                            <div className="j-left">
                                <div className="name">高汪</div>
                                <div className="qqqq">|</div>
                                <div className="identity">老板</div>
                            </div>
                            <div className="tel">18626870242</div>
                        </div> 
                    </div>
                    <div><img alt='' className="iconR" src={perR}/></div> 
                </div>
                <div className="content">
                <div className="main">
                    <div className="myleft">
                        <div><img alt='' src={per01}/></div>
                        <div>我的待办</div>
                    </div>
                    <div><img alt='' className="iconR" src={perR}/></div> 
                </div>
                <div className="main">
                    <div className="myleft">
                        <div><img alt='' src={per02}/></div>
                        <div>项目统计</div>
                    </div>
                    <div><img alt='' className="iconR" src={perR}/></div>
                </div>
                <div className="main">
                    <div className="myleft">
                        <div><img alt='' src={per03}/></div>
                        <div>合同收款确定</div>
                    </div>
                    <div><img alt='' className="iconR" src={perR}/></div>
                </div>
                <div className="main">
                    <div className="myleft">
                        <div><img alt='' src={per04}/></div>
                        <div>调整公司设置</div>
                    </div>
                    <div><img alt='' className="iconR" src={perR}/></div>
                </div>
                <div className="combine">
                    <div className="main tip" onClick={this.setMeterials}>
                        <div className="myleft">
                            <div><img alt='' src={per05}/></div>
                            <div>设置建材库</div>
                        </div>
                        <div><img alt='' className="iconR" src={perR}/></div>  
                    </div>
                    <div className="main">
                        <div className="myleft">
                            <div><img alt='' src={per06}/></div>
                            <div>设置部门和员工</div>
                        </div>
                    <div><img alt='' className="iconR" src={perR}/></div>   
                    </div>
                </div>
                <div className="main">
                    <div className="myleft">
                        <div><img alt='' src={per07}/></div>
                        <div>设置启动时广告业</div>
                    </div>
                    <div><img alt='' className="iconR" src={perR}/></div>
                </div>
                <div className="combine">
                    <div className="main tip">
                        <div className="myleft">
                            <div><img alt='' src={per08}/></div>
                            <div>装内助信用分</div>
                        </div>
                    <div><img alt='' className="iconR" src={perR}/></div>
                    </div>
                    <div className="main tip">
                        <div className="myleft">
                            <div><img alt='' src={per09}/></div>
                            <div>申请增加成员数或人工客服</div>
                        </div>
                        <div><img alt='' className="iconR" src={perR}/></div>
                    </div>
                    <div className="main">
                        <div className="myleft">
                            <div><img alt='' src={per10}/></div>
                            <div>申请增加业主数</div>
                        </div>
                        <div><img alt='' className="iconR" src={perR}/></div> 
                    </div>
                </div>
                <div className="combine">
                    <div className="main tip">
                        <div className="myleft">
                            <div><img alt='' src={per11}/></div>
                            <div>产品使用教程</div>
                        </div>
                        <div><img alt='' className="iconR" src={perR}/></div> 
                    </div>
                    <div className="main tip">
                        <div className="myleft">
                            <div><img alt='' src={per12}/></div>
                            <div>系统设置</div>
                        </div>
                        <div><img alt='' className="iconR" src={perR}/></div>
                    </div>
                    <div className="main tip">
                        <div className="myleft">
                            <div><img alt='' src={per13}/></div>
                            <div>一键分享装内助App给朋友</div>
                        </div>
                        <div><img alt='' className="iconR" src={perR}/></div> 
                    </div>
                    <div className="main">
                        <div className="myleft">
                            <div><img alt='' src={per14}/></div>
                            <div>意见反馈</div>
                        </div>
                        <div><img alt='' className="iconR" src={perR}/></div>
                    </div>
                </div>
                <div className="main">
                    <div className="myleft">
                        <div><img alt='' src={per15}/></div>
                        <div>注册一个独立新公司</div>
                    </div>
                    <div><img alt='' className="iconR" src={perR}/></div>
                </div>
                </div>
            </div>
        );
    }
}
